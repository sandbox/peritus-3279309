<?php

namespace Drupal\mediarecorder\Plugin\Field\FieldWidget;

use Drupal\file\Plugin\Field\FieldWidget\FileWidget;

/**
 * Plugin implementation of the 'file_mediarecorder' widget.
 *
 * @FieldWidget(
 *   id = "file_mediarecorder",
 *   label = @Translation("MediaRecorder"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class MediaRecorderWidget extends FileWidget {

}
