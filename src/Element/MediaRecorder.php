<?php

namespace Drupal\mediarecorder\Element;

use Drupal\Core\Render\Element\FormElement;
use Drupal\file\Element\ManagedFile;

/**
 * Provides a mediarecorder element.
 *
 * @FormElement("mediarecorder")
 */
class MediaRecorder extends ManagedFile {
}
